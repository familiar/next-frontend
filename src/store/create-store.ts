import { createStore, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk'
import rootReducer, { AppState } from '../reducers'

// Import the initial states
import { initialState as createPostState } from '../reducers/create-post'
import { initialState as loggedInUserState } from '../reducers/logged-in-user'
import { initialState as registerUserState } from '../reducers/create-user'
import { initialState as postsState } from '../reducers/posts'
import { initialState as preferencesState } from '../reducers/preferences'

function createMiddlewares() {
  const middlewares = [thunkMiddleware]

  return middlewares
}

const defaultState: AppState = {
  createPost: createPostState,
  loggedInUser: loggedInUserState,
  posts: postsState,
  preferences: preferencesState,
  registerUser: registerUserState
}

const initStore = (initialState: AppState = defaultState, options: any) => {
  const middlewares = createMiddlewares()
  const { isServer, req, res } = options

  if (isServer && req && res) {
    initialState.loggedInUser = {
      isAuthenticated: req.session!.user? true : false,
      user: req.session.user
    }

    if (initialState.loggedInUser.user && initialState.loggedInUser.user.languages) {
      initialState.preferences.languages = initialState.loggedInUser.user.languages
    } else if (req.cookies.preferences && req.cookies.preferences) {
      try {
        initialState.preferences = JSON.parse(req.cookies.preferences)
      } catch (e) {
        console.error('Unable to parse preferences', e)
      }
    }
  }

  return createStore(rootReducer, initialState, compose(applyMiddleware(...middlewares)))
}

export default initStore
