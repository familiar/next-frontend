import { ThunkDispatch, ThunkAction } from 'redux-thunk'
import { Action as ReduxAction } from 'redux'
import { AppState } from '../reducers'

export type Action = ReduxAction<string>
export type Dispatch = ThunkDispatch<AppState, any, Action>
export type ThunkAction<R> = ThunkAction<R, AppState, any, Action>

export interface DispatchProp {
  dispatch: Dispatch
}
