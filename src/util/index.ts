import { IncomingMessage, IncomingHttpHeaders } from 'http'

export function forwardedHeaders(req: IncomingMessage): IncomingHttpHeaders {

  const headers: IncomingHttpHeaders = { ...req.headers }

  if (req.connection.remoteAddress) {
    headers['X-Forwarded-For'] = req.headers['X-Forwarded-For']
    ? req.headers['X-Forwarded-For'] + `,${req.connection.remoteAddress}`
    : req.connection.remoteAddress
  }

  return headers
}
