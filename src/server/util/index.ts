import * as R from 'ramda'

export const isNilOrEmpty = R.either(R.isNil, R.isEmpty)
export const notNil = R.compose(R.not, R.isNil)