import config from '../configs'
import { Pool } from 'pg'
import * as channelService from '../api/services/channel'
import * as postService from '../api/services/post'
import * as userService from '../api/services/user'
import { setDBPool } from '../db';

(async () => {
  const pool = new Pool({
    database: config.db.database,
    host: config.db.host,
    password: config.db.password,
    port: config.db.port as number,
    user: config.db.user
  })
  setDBPool(pool);

  // Create default user
  const user = await userService.create('devuser', 'dev@localhost.com', 'abcd1234')
    .catch((err) => console.warn(`Error creating user: ${err.message}`))

  const userId = user && user.id || 1

  // Create default channel
  const channelId = await channelService.createChannel({
    country: 'my',
    description: 'All things malaysia',
    languages: ['en', 'zh', 'mz'],
    name: 'malaysia',
    region: 'southeastasia'
  })
  console.log('Created default channel.')

  const posts: postService.CreatePostInput[] = [
    { channelId, userId, title: 'Decision to freeze toll hikes will cost govt RM994.43mil - Nation | The Star Online', language: 'en', externalurl: 'https://www.thestar.com.my/news/nation/2018/12/27/decision-to-freeze-toll-hikes-will-cost-govt-rm994dot43mil/', image: 'https://www.thestar.com.my/~/media/online/2018/01/02/05/11/plus-highway2.ashx/?w=620&h=413&crop=1&hash=4353D6FE03B5963EEF5A70369D395F9AFF6A2CDF' },
    { channelId, userId, title: '贪官这么多！印尼年底炒2357公仆', language: 'zh', externalurl: 'https://chinese.malaymail.com/chinese/world/article/20180914-indonesia-corrupt-civil-servants-dismissed-in-december' },
    { channelId, userId, title: '火箭支持安华上阵波德申 林冠英：这是迟来的正义', language: 'zh', externalurl: 'https://chinese.malaymail.com/chinese/malaysia/article/20180915-dap-endorses-pkrs-pd-move', image: 'https://media.malaymail.com/resize_cache/uploads/articles/2018/2018-09/20180911KE4_lim_guan_eng-seo.JPG' },
    { channelId, userId, title: '刘特佐委律师　阻2新书全球发售', language: 'zh', externalurl: 'http://www.orientaldaily.com.my/s/259835#', image: 'http://media5.b-cdn.net/images/uploads/news/2018/SEP_2018/20180915/890%E2%80%98.jpg' },
    { channelId, userId, title: '苹果官网忽然下架　iPhone X上市不到一年成绝版', language: 'zh', externalurl: 'http://www.orientaldaily.com.my/s/259544#' },
    { channelId, userId, title: 'Dr M: Umno will not be allowed to join PH', language: 'en', externalurl: 'https://www.nst.com.my/news/nation/2018/09/411421/dr-m-umno-will-not-be-allowed-join-ph', image: 'https://assets.nst.com.my/images/articles/14JSPakatan2_NSTfield_image_socialmedia.var_1536929973.jpg' },
    { channelId, userId, title: 'Apple Watch Series 4 may save lives one day', language: 'en', externalurl: 'https://www.nst.com.my/lifestyle/bots/2018/09/411059/apple-watch-series-4-may-save-lives-one-day' },
    { channelId, userId, title: 'New iPhone XS, XS Max and XR set new trend in smartphone', language: 'en', externalurl: 'https://www.nst.com.my/lifestyle/bots/2018/09/410847/new-iphone-xs-xs-max-and-xr-set-new-trend-smartphone' },
    { channelId, userId, title: 'China tidak akan tunduk tuntutan rundingan Amerika', language: 'ms', externalurl: 'http://www.utusan.com.my/berita/luar-negara/china-tidak-akan-tunduk-tuntutan-rundingan-amerika-1.747771', image: 'http://www.utusan.com.my/polopoly_fs/1.747770!/image/image.jpg_gen/derivatives/landscape_650/image.jpg' },
    { channelId, userId, title: 'Perang ‘kedaulatan’ Beijing-Washington', language: 'ms', externalurl: 'http://www.utusan.com.my/berita/luar-negara/perang-8216-kedaulatan-8217-beijing-washington-1.446639', image: 'http://www.utusan.com.my/polopoly_fs/1.446637!/image/image.jpg_gen/derivatives/landscape_650/image.jpg' }
  ]

  await Promise.all(posts.map((post) => postService.createPost(post)))
  console.log(`Created ${posts.length} posts.`)
  await pool.end()
})()
  .catch((err) => console.error(err))

