# Server API
API endpoints to support frontend and admin operations.

## Testing the API locally
1. Install HTTPie from https://httpie.org/
2. Set the API key and user
```bash
export ADMIN_API_KEY=adminapikey
export ADMIN_API_USERNAME=devuser
```

## Admin API
### Creating post via admin API
```bash
http POST :3000/api/admin/post \
admin-api-key:$ADMIN_API_KEY \
admin-api-username:$ADMIN_API_USERNAME \
< data/create-post-body.json
```

### Get post via admin API
```bash
http GET :3000/api/admin/post/1 \
admin-api-key:$ADMIN_API_KEY
```

### Set post thumbnail
```bash
http POST :3000/api/admin/post/thumbnail \
admin-api-key:$ADMIN_API_KEY \
< data/set-thumbnail-body.json
```

### Check existence url via admin API
Make sure the query url is encoded(Can use encodeUriComponent)

```bash
http :3000/api/admin/contents/url?url=http%3A%2F%2Fwww.bernama.com%2Fbm%2Fdunia%2Fnews.php%3Fid%3D1642648 \
admin-api-key:$ADMIN_API_KEY
```