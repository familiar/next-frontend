import { Request, Response, NextFunction } from "express"
import * as R from 'ramda'
import { HEADER_ADMIN_API_KEY } from '../headers'

interface Config {
  apiKey: string
}

export default function (config: Config) {
  return (req: Request, res: Response, next: NextFunction) => {
    const apiKey = R.pathOr(undefined, ['headers', HEADER_ADMIN_API_KEY], req)

    if (apiKey !== config.apiKey) {
      return res.sendStatus(401)
    }

    return next()
  } 
}
