import * as R from 'ramda'
import { Request, Response } from 'express'
import { HEADER_ADMIN_API_USERNAME } from '../headers'
import { isNilOrEmpty } from '../../util'
import { LoggedInUser, findByUsername } from '../services/user'
import * as channelService from '../services/channel'
import * as postService from '../services/post'
import Notifier from '../lib/notifier'
import { validateUrl, validateHttpsUrl } from '../../validators'

// Currently only have one channel
const ALLOWED_CHANNEL = 'malaysia'

interface PostControllerOptions {
  notifier: Notifier
}

export default class PostController {
  private options: PostControllerOptions

  constructor(opt: PostControllerOptions) {
    this.options = opt
  }

  public async createPost(req: Request, res: Response) {
    if (!req.session || !req.session.user) {
      return res.sendStatus(401)
    }
  
    try {
      this.validateCreatePostBody(req.body)
    } catch (e) {
      return res.status(400).json({ message: e.message })
    }
  
    try {
      const { notifier } = this.options
      const user = req.session.user as LoggedInUser
      const { url, language, title, channelName, image } = req.body
  
      const channel = await channelService.findChannelByName(channelName)
  
      if (!channel) {
        return res.status(400).json({ message: 'Invalid channel' })
      }

      const imageUrl = validateHttpsUrl(image) ? image : undefined
      const postId = await postService.createPost({
        channelId: channel.id,
        externalurl: url,
        image: imageUrl,
        language,
        title,
        userId: user.id
      })

      try {
        await notifier.notifyPostCreated(postId)
      } catch (e) {
        console.warn('Error notifying post creation event', e)
      }
  
      return res.status(200).json({ postId })
    } catch (e) {
      console.error(e)
      res.sendStatus(500)
    }
  }

  public async getLatest (req: Request, res: Response) {
    try {
      const userId = req.session && req.session.user ? req.session.user.id : undefined
      const { languages } = req.query
      const result = await postService.getLatest(userId, languages)
      if (result) {
        res.json({ success: true, posts: result })
      } else {
        res.sendStatus(500)
      }
    } catch (e) {
      console.error(e)
      res.sendStatus(500)
    }
  }

  public async getPopular (req: Request, res: Response) {
    try {
      const userId = req.session && req.session.user ? req.session.user.id : undefined
      const { languages } = req.query
      const result = await postService.getPopular(userId, languages)
      if (result) {
        res.json({ success: true, posts: result })
      } else {
        res.sendStatus(500)
      }
    } catch (e) {
      console.error(e)
      res.sendStatus(500)
    }
  }

  public async vote (req: Request, res: Response) {
    try {
      if (!req.session || !req.session.user) {
        return res.sendStatus(401)
      }
  
      const user = req.session.user as LoggedInUser
      const { postId, vote: currentVote } = req.body
  
      if (isNilOrEmpty(postId) || isNilOrEmpty(currentVote)) {
        return res.status(400).json({ message: 'Missing requirement fields' })
      }
  
      await postService.vote({ postId, userId: user.id, vote: currentVote })
      return res.status(200)
    } catch (e) {
      console.error(e)
      res.sendStatus(500)
    }
  }

  // Handler for admin API endpoints
  public async createPostAdmin (req: Request, res: Response) {
    const username = R.pathOr(undefined, ['headers', HEADER_ADMIN_API_USERNAME], req)

    if (!username) {
      return res.sendStatus(400)
    }

    try {
      this.validateCreatePostBody(req.body)
    } catch (e) {
      return res.status(400).json({ message: e.message })
    }

    try {
      const { notifier } = this.options
      const { url, language, title, channelName, image } = req.body
      const channel = await channelService.findChannelByName(channelName)

      if (!channel) {
        return res.status(400).json({ message: 'Invalid channel' })
      }
      
      const user = await findByUsername(username)

      if (!user) {
        return res.status(400).json({ message: 'Username not found' })
      }

      const imageUrl = validateHttpsUrl(image) ? image : undefined    
      const postId = await postService.createPost({
        channelId: channel.id,
        externalurl: url,
        image: imageUrl,
        language,
        title,
        userId: user.id
      })

      try {
        await notifier.notifyPostCreated(postId)
      } catch (e) {
        console.warn('Error notifying post creation event', e)
      }
    
      return res.status(200).json({ postId })
    } catch (e) {
      console.error(e)
      return res.sendStatus(500)
    }
  }

  public async getPostAdmin(req: Request, res: Response) {
    const postId = req.params.id

    if (!postId) {
      return res.sendStatus(400)
    }

    const post = await postService.getById(postId)
    return res.json({ success: true, post })
  }

  public async setThumbnailAdmin(req: Request, res: Response) {
    const { postId, enabled } = req.body

    if (!postId || !enabled) {
      return res.sendStatus(400)
    }

    await postService.updateThumbnail({ postId, enabled })
    return res.json({ success: true })
  }

  // Input validators
  protected validateCreatePostBody(body: any) {
    const { url, language, title, channelName } = body

    if (isNilOrEmpty(url) || isNilOrEmpty(language) || isNilOrEmpty(title) || isNilOrEmpty(channelName)) {
      throw new Error('Missing required fields')
    }

    if (!validateUrl(url)) {
      throw new Error('Invalid post url')
    }

    if (channelName !== ALLOWED_CHANNEL) {
      throw new Error('Invalid channel')
    }
  }
}
