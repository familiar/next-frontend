import { decodeStream } from 'iconv-lite'
import { Stream } from 'stream'
import { Request, Response } from 'express'
import axios, { AxiosError } from 'axios'
import { parseCharsetFromContentTypeHeader, findLanguageFromCharset } from '../../charset'
import { isNilOrEmpty } from '../../util'

// Recommended usage from metascraper author :\
// tslint:disable:no-var-requires
const metascraper = require('metascraper')([
  require('metascraper-title')(),
  require('metascraper-description')(),
  require('metascraper-lang')(),
  require('metascraper-image')(),
])
// tslint:enable:no-var-requires

interface FetchUrlMetadataResult {
  url: string
  title: string
  description: string
  language?: string
  charset?: string
  image?: string
}

export default class ContentController {
  public async fetchUrlMetadata (req: Request, res: Response) {
    try {
      const { url } = req.body
  
      if (isNilOrEmpty(url)) {
        return res.status(400).json({ message: 'Missing require parameters.' })
      }
  
      const externalRes = await axios.get<Stream>(url, {
        responseType: 'stream',
        timeout: 3000
      })
  
      const charset = parseCharsetFromContentTypeHeader(externalRes.headers['content-type'])
  
      // If the content type has charset specified, we will decode the body with that charset.
      const inputStream = charset
        ? externalRes.data.pipe(decodeStream(charset))
        : externalRes.data
  
      const decodedBody = await new Promise<string>((resolve, reject) => {
        let body = ''
        inputStream.on('data', (data) => { body += data })
        inputStream.on('end', () => resolve(body))
        inputStream.on('error', (err) => reject(err))
      })
  
      const language = charset? findLanguageFromCharset(charset) : undefined
      const metadata = await metascraper({ html: decodedBody, url })
  
      const result: FetchUrlMetadataResult = {
        charset,
        description: metadata.description,
        image: metadata.image,
        language: metadata.lang || language,
        title: metadata.title,
        url
      }
  
      // Normalize language code for Chinese simplified and traditional
      if (result.language === 'zh-Hans' || result.language === 'zh-Hant') {
        result.language = 'zh'
      }
  
      return res.json(result)
    } catch (e) {
      if (e.response) {
        const axiosErr = e as AxiosError
        if (axiosErr.code === 'ECONNABORTED') {
          return res.sendStatus(408) // request timed out
        }
      }
  
      // unknown error
      console.error(e)
      return res.sendStatus(500)
    }
  }
}


