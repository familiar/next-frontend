import { Request, Response} from 'express'
import * as userService from '../services/user'
import { isNilOrEmpty } from '../../util'

export default class UserController {
  public async create (req: Request, res: Response) {
    try {
      const { username, email, password } = req.body
  
      if (isNilOrEmpty(username) || isNilOrEmpty(email) || isNilOrEmpty(password)) {
        return res.status(400).json({ message: 'Missing required fields or field cannot be empty' })
      }
  
      const result = await userService.create(username, email, password)
      if (result) {
        res.json({ user: result })
      } else {
        res.sendStatus(500)
      }
    } catch (e) {
      console.error(e)
      res.sendStatus(500)
    }
  }
  
  public async login (req: Request, res: Response) {
    try {
      const { email, password } = req.body
      
      if (isNilOrEmpty(email) || isNilOrEmpty(password)) {
        return res.status(400).json({ message: 'Missing required fields or field cannot be empty' })
      }
  
      const user = await userService.login(email, password)
      if (user) {
        req.session!.user = user
        res.json({ user })
      } else {
        res.sendStatus(401)
      }
    } catch (e) {
      console.error(e)
      res.sendStatus(500)
    }
  }
  
  public async logout (req: Request, res: Response) {
    if (req.session) {
      req.session.destroy((e) => {
        if (e) {
          console.error(e)
          return res.sendStatus(500)
        }
  
        return res.sendStatus(200)
      })
    } else {
      return res.sendStatus(200)
    }
  }
  
  public async setLanguages (req: Request, res: Response) {
    const user = req.session && req.session.user
  
    if (!user) {
      return res.sendStatus(401)
    }
  
    try {
      const { languages } = req.body
  
      if (!languages) {
        return res.status(400).json({ message: 'Missing required fields or field cannot be empty' })
      }
  
      await userService.setLanguages(user.id, languages)
      req.session!.user.languages = languages
      return res.json({ success: true })
    } catch (e) {
      console.error(e)
      res.sendStatus(500)
    }
  }
}
