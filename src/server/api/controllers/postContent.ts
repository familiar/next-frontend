import { Request, Response } from 'express'
import { isNilOrEmpty } from '../../util'
import { findPostContentByUrl } from '../services/postContent'

export default class PostContentController {
  public async checkExistenceUrl (req: Request, res: Response) {
    try {
      const { url } = req.query
  
      if (isNilOrEmpty(url)) {
        return res.status(400).json({ message: 'Missing requirement fields' })
      }
  
      const decodedUrl = decodeURIComponent(url)
      const postContent = await findPostContentByUrl(decodedUrl)
  
      if (postContent) {
        return res.sendStatus(200)
      }
  
      res.sendStatus(404)
    } catch (e) {
      console.error(e)
      return res.sendStatus(500)
    }
  }
}
