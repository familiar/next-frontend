import * as express from 'express'
import * as bodyParser from 'body-parser'
import UserController from './controllers/user'
import PostController from './controllers/post'
import ContentController from './controllers/content'
import PostContentController from './controllers/postContent'
import adminAuth from './middlewares/admin-auth'
import Notifier from './lib/notifier'

interface RouterOptions {
  adminApiKey: string
  notifier: Notifier
}

export default (config: RouterOptions) => {
  const router = express.Router()
  const { notifier } = config

  router.use(bodyParser.json())

  const userController = new UserController()
  const postController = new PostController({ notifier })
  const contentController = new ContentController()
  const postContentController = new PostContentController()

  router.post('/login', userController.login.bind(userController))
  router.post('/logout', userController.logout.bind(userController))
  router.post('/signup', userController.create.bind(userController))
  router.put('/user/languages', userController.setLanguages.bind(userController))

  router.post('/post', postController.createPost.bind(postController))
  router.get('/posts/latest', postController.getLatest.bind(postController))
  router.get('/posts/popular', postController.getPopular.bind(postController))
  router.post('/vote', postController.vote.bind(postController))

  router.post('/url-metadata', contentController.fetchUrlMetadata.bind(contentController))

  // Routes for admin API
  const adminRouter = express.Router()
  adminRouter.use(adminAuth({ apiKey: config.adminApiKey }))
  adminRouter.get('/post/:id', postController.getPostAdmin.bind(postController))
  adminRouter.post('/post', postController.createPostAdmin.bind(postController))
  adminRouter.post('/post/thumbnail', postController.setThumbnailAdmin.bind(postController))
  adminRouter.get('/contents/url', postContentController.checkExistenceUrl.bind(postContentController))

  router.use('/admin', adminRouter)

  return router
}