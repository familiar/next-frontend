import * as bcrypt from 'bcrypt'
import * as R from 'ramda'
import { getDBPool } from '../../db'

interface User {
  id: number
  username: string
  email: string
  password: string
  created_date: string
  updated_date: string
  display_name?: string
  avatar_url?: string
  country?: string
  languages?: string[]
}

export interface LoggedInUser {
  id: number
  email: string
  username: string
  languages?: string[]
}

export const login = async (email: string, password: string): Promise<LoggedInUser|undefined> => {
  const pool = getDBPool()
  const sqlQuery = 'select * from users where email = $1 limit 1'
  const { rows } = await pool.query(sqlQuery, [email])

  const user = R.head<User>(rows)

  if (user !== undefined) {
    const isSame = await bcrypt.compare(password, user.password)
    if (isSame) {
      return R.pick(['email', 'id', 'username', 'languages'], user)
    }
  }

  return undefined
}

export const create = async (username: string, email: string, password: string) => {
  const pool = getDBPool()
  const hashedPassword = await bcrypt.hash(password, 10)
  const sqlQuery = 'INSERT INTO users(username, email, password) VALUES($1, $2, $3) RETURNING *'
  const { rows } = await pool.query(sqlQuery, [username, email, hashedPassword])
  const user: User = R.head<User>(rows)!
  return R.pick(['email', 'id', 'username', 'languages'], user)
}

export const setLanguages = async (userId: number, languages: string[]) => {
  const pool = getDBPool()
  const sql = 'UPDATE users SET languages = $1 WHERE id = $2'
  return pool.query(sql, [JSON.stringify(languages), userId])
}

export const findByUsername = async (username: string): Promise<LoggedInUser | undefined> => {
  const pool = getDBPool()
  const sqlQuery = 'SELECT * FROM users WHERE username = $1 LIMIT 1'
  const { rows } = await pool.query(sqlQuery, [username])
  const user = R.head<User>(rows)

  if (user) {
    return R.pick(['email', 'id', 'username', 'languages'], user)
  }
}
