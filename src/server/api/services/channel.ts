import { getDBPool } from '../../db'
import * as R from 'ramda'

export interface Channel {
  id: number
  name: string
  description: string
  createdDate: Date
  updatedDate: Date
  languages?: string[]
  country?: string
  region?: string
}

export interface CreateChannelInput {
  name: string,
  description: string,
  languages?: string[],
  country?: string,
  region?: string
}

/**
 * createChannel function creates a channel and return the channel ID.
 */
export const createChannel = async (input: CreateChannelInput): Promise<number> => {
  const pool = getDBPool()
  const sql = 'INSERT INTO channels (name, description, languages, country, region) '
    + 'VALUES ($1, $2, $3, $4, $5) '
    + 'RETURNING id'
  
  const res = await pool.query(sql, [input.name, input.description, JSON.stringify(input.languages), input.country, input.region]) 
  const row = R.head(res.rows)

  if (!row || !row.id) {
    throw new Error('Unable to create channel')
  }

  return row.id
}

/**
 * findChannelById function returns a channel object by channel ID.
 */
export const findChannelByName = async (name: string): Promise<Channel|undefined> => {
  const pool = getDBPool()
  const sql = 'SELECT id, name, description, languages, country, region, created_date, updated_date '
    + 'FROM channels WHERE name = $1'
  const res = await pool.query(sql, [name])
  return R.head<Channel>(res.rows)
}
