import * as R from 'ramda'
import { getDBPool } from '../../db'
import { isNilOrEmpty, notNil } from '../../util'

export interface Post {
  id: number
  channelId: number
  userId: number
  title: string
  points: number
  createdDate: Date
  updatedDate: Date
  language: string
  content: PostContent
}

export interface PostContent {
  id: number
  postId: number
  createdDate: Date
  externalUrl?: string
  body?: string
}

export interface CreatePostInput {
  channelId: number
  userId: number
  title: string
  language: string
  externalurl?: string
  body?: string
  image?: string
}

export interface VoteInput {
  postId: number
  vote: number
  userId: number
}

export interface GetPostOutput {
  id: number
  user_id: number,
  points: number,
  created_date: Date,
  updated_date: Date,
  language: string,
  image: string,
  title: string,
  external_url: string,
  body: string,
  images: string[],
  channel_name: string,
  channel_region: string
}

export interface UpdateThumbnailInput {
  postId: number
  enabled: boolean
}

// If post is older than N hours, give it 0 point.
const POPULAR_HOURS = 12


/**
 * createPost creates a Post and PostContent
 */
export const createPost = async (input: CreatePostInput): Promise<number> => {
  if (isNilOrEmpty(input.externalurl) && isNilOrEmpty(input.body)) {
    throw new Error('createPost requires either externalUrl or body specified.')
  }

  const pool = getDBPool()
  const client = await pool.connect()
  let postId: number

  try {
    await client.query('BEGIN')
    const { rows } = await client.query('INSERT INTO posts (channel_id, user_id, language, image) VALUES($1,$2,$3,$4) RETURNING id',
      [input.channelId, input.userId, input.language, input.image])
    postId = rows[0].id as number
    await client.query('INSERT INTO post_contents (post_id, title, external_url, body) VALUES($1,$2,$3,$4)', [postId, input.title, input.externalurl, input.body])
    await client.query('COMMIT')
  } catch (e) {
    await client.query('ROLLBACK')
    throw e
  } finally {
    client.release()
  }
  return postId
}

const sqlIfElse = (sql: string) => R.ifElse(notNil, R.always(sql), R.always(''))
const sqlParamList = (list: any[], start: number) => list.map((_, i) => `$${i + start}`).join(',')

export const getLatest = async (userId?: number, languages?: string[], limit: number = 100) => {
  const pool = getDBPool()
  const sqlQuery = 'select p.*, pc.title, pc.external_url, pc.images, pc.body, u.username'
    + sqlIfElse(', CASE WHEN pv.vote IS NULL THEN 0 ELSE pv.vote END AS vote')(userId)
    + ' from posts p'
    + ' join post_contents pc on pc.post_id = p.id'
    + ' join users u on u.id = p.user_id'
    + sqlIfElse(' left join post_votes pv on pv.post_id = p.id and pv.user_id = $1')(userId)
    + (!isNilOrEmpty(languages) ? ` where p.language in (${sqlParamList(languages!, userId ? 2 : 1)})` : '')
    + ' order by p.created_date desc'
    + ` LIMIT ${limit}`

  let params: any = []

  if (!isNilOrEmpty(userId)) {
    params.push(userId)
  }

  if (!isNilOrEmpty(languages)) {
    params = params.concat(languages)
  }

  const { rows } = await pool.query(sqlQuery, params)
  return rows
}

export const getPopular = async (userId?: number, languages?: string[], limit: number = 100) => {
  const pool = getDBPool()
  const sqlQuery = 'select p.*, pc.title, pc.external_url, pc.images, pc.body, u.username,'
    + ` CASE WHEN (now() - interval '${POPULAR_HOURS} hours' < p.created_date) THEN p.points ELSE 0 END AS actual_points`
    + sqlIfElse(', CASE WHEN pv.vote IS NULL THEN 0 ELSE pv.vote END AS vote')(userId)
    + ' from posts p'
    + ' join post_contents pc on pc.post_id = p.id'
    + ' join users u on u.id = p.user_id'
    + sqlIfElse(' left join post_votes pv on pv.post_id = p.id and pv.user_id = $1')(userId)
    + (!isNilOrEmpty(languages) ? ` where p.language in (${sqlParamList(languages!, userId ? 2 : 1)})` : '')
    + ' order by actual_points desc, p.created_date desc'
    + ` LIMIT ${limit}`

    let params: any = []

    if (!isNilOrEmpty(userId)) {
      params.push(userId)
    }

    if (!isNilOrEmpty(languages)) {
      params = params.concat(languages)
    }

    const { rows } = await pool.query(sqlQuery, params)
    return rows
}

export const vote = async (input: VoteInput) => {
  const pool = getDBPool()
  const { postId, userId, vote: currentVote } = input
  const client = await pool.connect()

  try {
    await client.query('BEGIN')
    const { rows } = await client.query('SELECT * FROM post_votes WHERE post_id = $1 AND user_id = $2', [postId, userId])

    await client.query(
      'INSERT INTO post_votes (post_id, user_id, vote) VALUES($1, $2, $3) ON CONFLICT (post_id, user_id) DO UPDATE SET vote = $3, UPDATED_DATE = NOW()',
      [postId, userId, currentVote]
    )

    const prevVote = R.isEmpty(rows) ? 0 : rows[0].vote
    const voteDiff = currentVote - prevVote
    const pointOperation = voteDiff >= 0 ? `+ ${voteDiff}` : `- ${Math.abs(voteDiff)}`

    await client.query(`UPDATE posts SET points = points ${pointOperation} WHERE id = $1`, [postId])
    await client.query('COMMIT')
  } catch (e) {
    await client.query('ROLLBACK')
    throw e
  } finally {
    client.release()
  }
}

export const getById = async (postId: number): Promise<GetPostOutput | undefined> => {
  const pool = getDBPool()
  const sqlQuery = 'SELECT p.id, p.user_id, p.points, p.created_date, p.updated_date, ' +
    'p.language, p.image, pc.title title, pc.external_url, pc.body, pc.images images, ' +
    'c.name channel_name, c.region channel_region ' +
    'FROM posts p LEFT JOIN post_contents pc ON pc.post_id = p.id ' +
    'LEFT JOIN channels c on c.id = p.channel_id where p.id = $1;'
  
  const { rows } = await pool.query(sqlQuery, [postId])
  
  if (rows.length === 0) {
    return undefined
  }

  return rows[0] as GetPostOutput
}

export const updateThumbnail = async (input: UpdateThumbnailInput): Promise<void> => {
  const pool = getDBPool()
  const sqlQuery = 'UPDATE posts SET has_thumbnail = $1 WHERE id = $2'
  await pool.query(sqlQuery, [input.enabled, input.postId])
}
