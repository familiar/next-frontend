import { getDBPool } from '../../db'
import * as R from 'ramda'

export interface PostContent {
  id: number
  title: string
  post_id: number
  created_date: Date
  external_url?: string
  body?: string
}

/**
 * Find postContent by external url
 *
 * @param {string} url - external url
 * @return {PostContent|undefined} - PostContent if found, otherwise undefined
 */
export const findPostContentByUrl = async (url: string): Promise<PostContent|undefined> => {
  const pool = getDBPool()
  const sql = 'SELECT id, title, post_id, external_url, created_date FROM post_contents WHERE external_url = $1'
  const res = await pool.query(sql, [url])
  return R.head<PostContent>(res.rows)
}
