import { SNS } from 'aws-sdk'

const AWS_REGION = 'ap-southeast-1'

export interface NotifierOptions {
    postCreatedSNSTopic: string
}

export default class Notifier {
    private options: NotifierOptions
    private sns: SNS

    constructor(opt: NotifierOptions) {
        this.options = opt
        this.sns = new SNS({ region: AWS_REGION })
    }

    public async notifyPostCreated(postId: number) {
        return this.sns.publish({
            Message: JSON.stringify({ postId }),
            TopicArn: this.options.postCreatedSNSTopic
        }).promise()
    }
}
