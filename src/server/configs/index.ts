import { SSM } from 'aws-sdk'

export interface Config {
  db: {
    database: string
    host: string
    port: number | string
    password: string
    user: string
  }
  redis: {
    host: string
    port: number | string
  }
  sessionSecret: string
  adminApiKey: string
  aws: {
    sns: {
      postCreatedTopicArn: string
    }
  }
}

const config: Config = {
  adminApiKey: process.env.apiKey || 'adminapikey',
  aws: {
    sns: {
      postCreatedTopicArn: process.env.POST_CREATED_TOPIC_ARN ||  'arn:aws:sns:ap-southeast-1:935005516588:post-created-development'
    }
  },
  db: {
    database: process.env.DB_NAME || 'familiar',
    host: process.env.DB_HOST || 'localhost',
    password: process.env.DB_PASSWORD || 'dbpassword',
    port: process.env.DB_PORT || 5431,
    user: process.env.DB_USER || 'dbuser',
  },
  redis: {
    host: process.env.REDIS_HOST || 'localhost',
    port: process.env.REDIS_PORT || 6378
  },
  sessionSecret: process.env.SESSION_SECRET || 'anything'
}

export async function initConfig(): Promise<Config> {
  const { NODE_ENV = 'development' } = process.env

  if (NODE_ENV !== 'development') {
    console.log('Fetch secrets')
    config.adminApiKey = await getSecretValue('FamiliarAdminAPIKey')
    config.db.password = await getSecretValue('FamiliarDBUserPassword')
    config.sessionSecret = await getSecretValue('FamiliarSessionSecret')
  }

  return config
}

async function getSecretValue (name: string): Promise<string> {
  const ssm = new SSM({ region: 'ap-southeast-1' })
  return ssm.getParameter({ Name: name, WithDecryption: true })
  .promise()
  .then(({ Parameter }) => {
    if (!Parameter || !Parameter.Value) {
      throw new Error('Parameter not found')
    }
    return Parameter.Value
  })
}

export default config