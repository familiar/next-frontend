interface LanguageToCharsetsMap {
  [index: string]: string
}

interface CharsetToLanguageMap {
  [index: string]: string
}

// List is copied from node-iconv documentation: https://www.npmjs.com/package/iconv#supported-encodings
const languageToCharsets: LanguageToCharsetsMap = {
  jp: 'EUC-JP, SHIFT_JIS, CP932, ISO-2022-JP, ISO-2022-JP-2, ISO-2022-JP-1, EUC-JISX0213, Shift_JISX0213, ISO-2022-JP-3',
  kr: 'EUC-KR, CP949, ISO-2022-KR, JOHAB',
  la: 'MuleLao-1, CP1133',
  th: 'ISO-8859-11, TIS-620, CP874, MacThai',
  vn: 'VISCII, TCVN, CP1258',
  zh: 'EUC-CN, HZ, GBK, CP936, GB18030, EUC-TW, BIG5, CP950, BIG5-HKSCS, BIG5-HKSCS:2004, BIG5-HKSCS:2001, BIG5-HKSCS:1999, ISO-2022-CN, ISO-2022-CN-EXT, BIG5-2003'
}

const charsetToLanguage: CharsetToLanguageMap = {}

Object.keys(languageToCharsets).forEach((language) => {
  const charsets = languageToCharsets[language].toLowerCase().split(', ')
  charsets.forEach((charset) => {
    charsetToLanguage[charset] = language
  })
})

export { languageToCharsets, charsetToLanguage }