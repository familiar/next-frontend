import { charsetToLanguage } from './charsets'

export interface ParseResult {
  charset?: string
  language?: string
  text: string
}

const charsetParserRegex = /charset=([\-a-z0-9]+);?/

export function parseCharsetFromContentTypeHeader(contentType: string): string | undefined {
  const matches = contentType.match(charsetParserRegex)
  if (matches) {
    return matches[1]
  }
}

export function findLanguageFromCharset(charset: string): string | undefined {
  return charsetToLanguage[charset]
}
