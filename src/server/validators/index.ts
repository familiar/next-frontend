const httpsUrlRegex = /https?:\/\/[(www\.)?a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/
const urlRegex = /[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/

export function validateUrl(url: string): boolean {
  return urlRegex.test(url)
}

export function validateHttpsUrl(url: string): boolean {
  return httpsUrlRegex.test(url)
}
