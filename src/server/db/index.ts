import { Pool } from 'pg';

let dbPool: Pool | undefined

export function setDBPool (pool: Pool) {
  dbPool = pool
}

export function getDBPool (): Pool {
  if (!dbPool) {
    throw new Error('Database pool has not been registered')
  }
  return dbPool
}
