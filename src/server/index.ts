import * as http from 'http'
import { Request, Response } from 'express'
import { Pool } from 'pg'
import { createTerminus } from '@godaddy/terminus'
import apiRouter from './api'
import { setDBPool } from './db'
import { initConfig, Config } from './configs'
import Notifier from './api/lib/notifier';

// This is a default boilerplate for server to check if Typescript compiles `src` properly
// This can be swapped to expressjs in future.

// tslint:disable:no-var-requires
const next = require('next')
const express = require('express')
const cookieParser = require('cookie-parser')
const session = require('express-session')
const RedisStore = require('connect-redis')(session)
// tslint:enable:no-var-requires

const port = parseInt(process.env.PORT || '3000', 10)
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare()
  .then(initConfig)
  .then((config: Config) => {
    // Initialize the database pool
    const dbPool = new Pool({
      database: config.db.database,
      host: config.db.host,
      password: config.db.password,
      port: config.db.port as number,
      user: config.db.user
    })
    setDBPool(dbPool)

    const appE = express()
    appE.disable('x-powered-by')
    appE.enable('trust proxy')
    appE.use(cookieParser())
    appE.use(session({
      cookie: {
        domain: 'familiar.one',
        maxAge: 86400000 * 7, // 7 days
        secure: !dev
      },
      name: 'ssid',
      resave: false,
      saveUninitialized: false,
      secret: config.sessionSecret,
      store: new RedisStore({
        host: config.redis.host,
        port: config.redis.port
      })
    }))

    const notifier = new Notifier({
      postCreatedSNSTopic: config.aws.sns.postCreatedTopicArn
    })

    appE.use('/api', apiRouter({
      adminApiKey: config.adminApiKey,
      notifier
    }))

    appE.get('/l', (req: Request, res: Response) => {
      // endpoint for link redirection
      return app.render(req, res, '/redirection', req.query)
    })

    appE.get('/health', (req: Request, res: Response) => {
      res.sendStatus(200)
    })

    appE.get('*', (req: Request, res: Response) => {
      return handle(req, res) 
    })

    const server = http.createServer(appE)

    createTerminus(server, {
      onSignal: async () => {
        console.log('Clean up resources on SIGTERM')
        const { pool } = require('./db')
        return pool.end()
      }
    })

    server.listen(port, (err: any) => {
      if (err) {
        throw err
      }
      console.log(`> Ready on http://localhost:${port}`)
  })
})
