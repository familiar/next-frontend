import { Menu, Icon, Row } from 'antd'
import * as React from 'react'
import SiteLogo from './SiteLogo'
import { LoggedInUserState } from '../reducers/logged-in-user'

const { SubMenu, ItemGroup } = Menu 

export interface NavbarProp {
  selectedPath: string;
  loggedInUser: LoggedInUserState;
}

const Navbar = (prop: NavbarProp) => (
  <Row>
    <Menu
      mode="horizontal"
      selectedKeys={[prop.selectedPath]}
    >
      <SiteLogo />
      <Menu.Item key="/">
        <a href="/"><Icon type="like-o" /> Popular Posts</a>
      </Menu.Item>
      <Menu.Item key="/latest">
        <a href="/latest"><Icon type="to-top" /> Latest Posts</a>
      </Menu.Item>
      {
        prop.loggedInUser.isAuthenticated && prop.loggedInUser.user && (
          <SubMenu title={<span className="submenu-title-wrapper"><Icon type="user" /> {prop.loggedInUser.user.username}</span>}>
            <Menu.Item key="/logout">
              <a href="/logout">Logout</a>
            </Menu.Item>
          </SubMenu>
        )
      }
    </Menu>
  </Row>
)

export default Navbar