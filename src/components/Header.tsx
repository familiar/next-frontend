import styled from 'styled-components'
import { Layout } from 'antd'

const { Header } = Layout

const WhiteHeader = styled(Header)`
  background: #ffffff
`

export default WhiteHeader
