import styled from 'styled-components'
import { Layout } from 'antd'

const { Footer } = Layout

const WhiteFooter = styled(Footer)`
  background: #ffffff
`

export default WhiteFooter
