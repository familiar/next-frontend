import * as React from 'react'
import { Form, Input, Button, Radio, Spin, Row, Col } from 'antd'
import { FormComponentProps } from 'antd/lib/form'

const FormItem = Form.Item

interface CreatePostFormProp extends FormComponentProps {
  title: string
  url: string
  language: string
  creatingPost?: boolean
  fetchingUrlMetadata?: boolean
  createPostErrorMessage?: string
  onSubmit: () => void
  onUrlChanged: (url: string) => void
  onChange: (changedFields: any) => void
}

// Stateless form component. Component props are managed by parent component.
class CreatePostForm extends React.Component<CreatePostFormProp> {
  triggerUrlUpdateIfValid = () => {
    const { validateFields, getFieldValue } = this.props.form
    validateFields(['url'], (errors) => {
      if (!errors) {
        const url = getFieldValue('url')
        this.props.onUrlChanged(url)
      }
    })
  }

  handleUrlPasted = (event: React.ClipboardEvent) => {
    // trigger the input element's onChange event, by default pasting does not trigger it.
    window.setTimeout(() => this.triggerUrlUpdateIfValid())
  }

  handleSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    this.props.form.validateFields((err) => {
      if (!err) {
        this.props.onSubmit()
      }
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const formItemLayout = {
      labelCol: { span: 2},
      wrapperCol: { span: 12}
    }

    return (
      <Form layout="horizontal" onSubmit={ this.handleSubmit }>
        <FormItem label="URL" {...formItemLayout}>
          <Row gutter={ 8 }>
            <Col span={ this.props.fetchingUrlMetadata? 23 : 24 }>
            {
              getFieldDecorator('url', {
                rules: [
                  { required: true, message: 'Please input the page url.' },
                  { type: 'url', message: 'Please input valid url.'}
                ]
              })(<Input placeholder="http://" onChange={this.triggerUrlUpdateIfValid} onPaste={this.handleUrlPasted} />)
            }
            </Col>
            <Col span={ this.props.fetchingUrlMetadata? 1 : 0 }>
              <Spin spinning={ this.props.fetchingUrlMetadata } />
            </Col>
          </Row>
        </FormItem>
        <FormItem label="Title" {...formItemLayout}>
          {
            getFieldDecorator('title', {
              rules: [
                { required: true, message: 'Please input the post title.' }
              ]
            })(<Input placeholder="Post title" />)
          }
        </FormItem>
        <FormItem label="Language" {...formItemLayout}>
          {
            getFieldDecorator('language', {
              rules: [
                { required: true, message: 'Please select language.' }
              ]
            })(
              <Radio.Group>
                <Radio.Button value="en">English</Radio.Button>
                <Radio.Button value="zh">Chinese</Radio.Button>
                <Radio.Button value="ms">Malay</Radio.Button>
              </Radio.Group>
            )
          }
        </FormItem>
        <FormItem>
          <Row>
            <Button type="primary" htmlType="submit" disabled={this.props.creatingPost}>
              { this.props.creatingPost? 'Creating...' : 'Create Post'}
            </Button>
          </Row>
        </FormItem>
      </Form>
    )
  }
}

const WrappedCreatePostForm = Form.create({
  // Binding the component props to field values
  mapPropsToFields: (props: any) => ({
    language: Form.createFormField({
      value: props.language
    }),
    title: Form.createFormField({
      value: props.title
    }),
    url: Form.createFormField({
      value: props.url
    })
  }),
  // Call the callback function to let parent handle properties update
  onValuesChange: (props: any, changedFields: any) => {
    props.onChange(changedFields)
  }
})(CreatePostForm)
export default WrappedCreatePostForm
