import styled from 'styled-components'
import { Row, Col, Icon, Skeleton } from 'antd'
import * as React from 'react'
import { Post as PostType } from '../reducers/posts'
import { VoteIconText } from '../components/IconText'
import { distanceInWordsToNow } from 'date-fns'

interface PostProps {
  post: PostType
  loading?: boolean
  onUpvote: (postId: number) => void
  onDownvote: (postId: number) => void
}

const StyledPostItemRow = styled.div`
  padding: 12px 0;
`

const StyledPostTitle = styled.div`
  font-size: 16px;
  font-weight: 500;

  a {
    color: black;
  }

  a:hover {
    color: #1890ff;
  }
`

const StyledPostMedata = styled.div`
  font-size: 11px;
`

const StyledFlexContainer = styled.div`
  display: flex;
`

const StyledThumbnail = styled.div`
  margin-right: 10px;
`

const StyledImg = styled.img`
  height: 100%;
`

const urlPrefixRegexp = /^(https?)?:\/\/(www\.)?/i

function shortenUrl(url: string, limit: number = 70) {
  return url.replace(urlPrefixRegexp, '').substr(0, limit)
}

function createLinkUrl(post: PostType): string | undefined {
  if (post.external_url) {
    return `/l?u=${encodeURIComponent(post.external_url)}&p=${post.id}`
  }
}

export const Post = ({ onDownvote, onUpvote, post, loading }: PostProps) => {
  const timeFromNow = distanceInWordsToNow(post.created_date, { addSuffix: true });
  return (
    <StyledPostItemRow>
      {
        loading
        ? <Skeleton loading={loading} active={true} title={false} paragraph={{ rows: 3 }} />
        : <Row gutter={8}>
            <Col span={1}>
              <VoteIconText
                arrowDownFocus={post.vote < 0}
                arrowUpFocus={post.vote > 0}
                key={`vote-icon-text-${post.id}`}
                onDownvote={onDownvote}
                onUpvote={onUpvote}
                totalVote={post.points.toString()}
              />
            </Col>
            <Col span={23}>
              <StyledFlexContainer>
                <StyledThumbnail>
                  {post.image && <StyledImg src={post.image} width={'77px'} />}
                </StyledThumbnail>
                <div>
                  <StyledPostTitle>
                    {
                      post.external_url
                        ? <a href={createLinkUrl(post)} target="_blank">
                            <Icon type="link" /> {post.title}
                          </a>
                      : post.title
                    }
                  </StyledPostTitle>
                  <StyledPostMedata>
                    {
                      post.external_url
                        ? <span>
                            Source: <a href={createLinkUrl(post)} target="_blank">{shortenUrl(post.external_url)}</a>...
                          </span>
                        : ''
                    }<br />
                    Posted by {post.username} {timeFromNow}
                  </StyledPostMedata>
                </div>
              </StyledFlexContainer>
            </Col>
          </Row>
      }
    </StyledPostItemRow>
  )
}