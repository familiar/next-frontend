import { Form, Icon, Input, Button } from 'antd'
import * as React from 'react'
import Link from 'next/link'
import { FormComponentProps } from 'antd/lib/form'

const FormItem = Form.Item

interface SignUpFormProps extends FormComponentProps {
  registering: boolean
  onSubmit: (data: any) => void;
}

class SignUpForm extends React.Component<SignUpFormProps> {
  handleSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onSubmit(values)
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form

    return (
      <Form onSubmit={this.handleSubmit} className="signup-form">
        Sign Up with Familiar
        <FormItem>
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Please input your username!' }]
          })(<Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />)}
        </FormItem>
        <FormItem>
          {getFieldDecorator('email', {
            rules: [{ required: true, message: 'Please input your email!' }, { type: 'email', message: 'Invalid email address!' }]
          })(<Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />)}
        </FormItem>
        <FormItem>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your password!' }, { min: 8, message: 'Minimum 8 characters!' }]
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Password"
            />
          )}
        </FormItem>
        <FormItem>
          <Button type="primary" htmlType="submit" loading={this.props.registering}>
            Sign up
          </Button>
        </FormItem>
        Already a member? <Link prefetch={true} href="/login"><a>LOGIN</a></Link>
      </Form>
    )
  }
}

const WrappedSignUpForm = Form.create()(SignUpForm);
export default WrappedSignUpForm;