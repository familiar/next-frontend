import { Icon } from 'antd'
import * as React from 'react'
import styled from 'styled-components'

interface IconTextProps {
  onClick?: (...args: any[]) => any
  text?: string;
  type: string;
  className?: string;
}

interface VoteIconTextProps {
  totalVote: string;
  onDownvote: (...args: any[]) => any;
  onUpvote: (...args: any[]) => any;
  arrowUpFocus?: boolean
  arrowDownFocus?: boolean
}

export const IconText = ({ className, onClick, type, text }: IconTextProps) => (
  <span className={className}>
    <Icon onClick={onClick} type={type} />
    {text}
  </span>
);

interface StyledVoteIconTextProps {
  focus?: boolean
}

const StyledVoteIconText = styled(IconText)`
  color: ${(props: StyledVoteIconTextProps) => props.focus ? '#1890ff' : 'grey'};

  :hover {
    cursor: pointer;
  }
`

export const VoteIconText = ({ arrowUpFocus = false, arrowDownFocus = false, onDownvote, onUpvote, totalVote }: VoteIconTextProps) => (
  <div style={{ textAlign: 'center' }}>
    <div><StyledVoteIconText focus={arrowUpFocus} type="up" onClick={onUpvote} /></div>
    <div>{totalVote}</div>
    <div><StyledVoteIconText focus={arrowDownFocus} type="down" onClick={onDownvote} /></div>
  </div>
);