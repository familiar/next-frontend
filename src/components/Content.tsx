import styled from 'styled-components'
import { Layout } from 'antd'

const { Content } = Layout

const StyledContent = styled(Content)`
  padding: 0 50px;
  background: white;
`

export default StyledContent