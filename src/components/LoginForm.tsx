import { Form, Icon, Input, Button } from 'antd'
import * as React from 'react'
import Link from 'next/link'
import { FormComponentProps } from 'antd/lib/form'

const FormItem = Form.Item

interface LoginFormProps extends FormComponentProps {
  logining: boolean
  onSubmit: (data: any) => void
}

class LoginForm extends React.Component<LoginFormProps> {
  handleSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onSubmit(values)
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form

    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        Login
        <FormItem>
          {getFieldDecorator('email', {
            rules: [{ required: true, message: 'Please input your email!' }, { type: 'email', message: 'Invalid email address!' }]
          })(<Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />)}
        </FormItem>
        <FormItem>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your password!' }]
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
              placeholder="Password"
            />
          )}
        </FormItem>
        <FormItem>
          <Button type="primary" htmlType="submit" loading={this.props.logining}>
            Login
          </Button>
        </FormItem>
        New to Familiar? <Link prefetch={true} href="/register"><a>REGISTER</a></Link>
      </Form>
    )
  }
}

const WrappedLoginForm = Form.create()(LoginForm);
export default WrappedLoginForm;