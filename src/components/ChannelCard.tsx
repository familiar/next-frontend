import * as React from 'react'
import { Card, Divider, Checkbox, Row, Button } from 'antd'

interface Language {
  value: string
  label: string
}

interface ChannelCardProps {
  channelName: string
  channelDescription: string
  isAuthenticated?: boolean
  languages: Language[]
  defaultLanguage: string
  selectedLanguages?: string[]
  onLanguageChange: (checkedValues: any[]) => void
}

const channelCard = (props: ChannelCardProps) => (
  <Card>
    <h3>#{props.channelName}</h3>
    <p>{props.channelDescription}</p>
    {
      props.isAuthenticated
        ? <Button href="/create-post" type="primary">Create Post</Button>
        : <div><a href="/login">Login</a> or <a href="/register">register</a> to create post</div>
    }

    <Divider />

    <h4>Languages</h4>
    <Checkbox.Group
      onChange={props.onLanguageChange}
      value={props.selectedLanguages || [props.defaultLanguage]}
    >
      {props.languages.map((language) => (
        <Row key={language.value}>
          <Checkbox value={language.value}>{language.label}</Checkbox>
        </Row>
      ))}
    </Checkbox.Group>

    <Divider />

    <div style={{ fontSize: '11px' }}>
      <b>About Familiar</b><br/>
      Familiar is place where people can share and read updates happening in their community.
    </div>
  </Card>
)

export default channelCard