import styled from 'styled-components'

const StyledSiteLogo = styled.div`
  position: relative;
  top: 1px;
  padding: 0 15px;
  float: left;
  background-color: #1890ff;
  color: white;
`

const SiteLogo = () => (
  <StyledSiteLogo>Familiar</StyledSiteLogo>
)

export default SiteLogo
