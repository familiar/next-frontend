import { List } from 'antd'
import * as React from 'react'
import { Post } from './Post'
import { Posts } from '../reducers/posts'

interface PostListProps {
  postIds: number[];
  posts: Posts;
  loading?: boolean
  onDownvote: (postId: number) => void;
  onUpvote: (postId: number) => void;
}

export class PostList extends React.Component<PostListProps> {
  renderItem = (postId: number) => {
    const { onDownvote, onUpvote, posts, loading } = this.props
    return (
      <Post
        onDownvote={onDownvote.bind(null, postId)}
        onUpvote={onUpvote.bind(null, postId)}
        post={posts[postId]}
        loading={loading}
      />
    )
  }

  render() {
    const { postIds } = this.props
    return (
      <List
        itemLayout="vertical"
        dataSource={postIds}
        renderItem={this.renderItem}
      />
    )
  }
}