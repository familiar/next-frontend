import {
  URL_METADATA_REQUEST,
  SET_CREATE_POST_METADATA,
  URL_METADATA_RECEIVED,
  CREATE_POST_REQUEST,
  CREATE_POST_RECEIVED,
  CREATE_POST_FAILED
} from '../actions/action-types'

import {
  SetCreatePostValuesAction,
  CreatePostFailedAction,
  CreatePostAction,
  ReceiveUrlMetadataAction
} from '../actions/create-post'


export interface CreatePostState {
  fetchingMetadata?: boolean
  creatingPost?: boolean
  url?: string
  metadata?: {
    title: string
    language: string
    image?: string
  }
  createPostErrorMessage?: string
}

export const initialState: CreatePostState = {
  creatingPost: false,
  fetchingMetadata: false
}

const createPostReducer = (state: CreatePostState = initialState, action: CreatePostAction): CreatePostState => {
  switch (action.type) {
    case URL_METADATA_REQUEST:
      return {
        ...state,
        fetchingMetadata: true
      }
    case URL_METADATA_RECEIVED:
      return {
        ...state,
        fetchingMetadata: false,
        metadata: (action as ReceiveUrlMetadataAction).metadata
      }
    case SET_CREATE_POST_METADATA:
      const setCreatePostMetadataAction = (action as SetCreatePostValuesAction)
      return {
        ...state,
        metadata: setCreatePostMetadataAction.metadata,
        url: setCreatePostMetadataAction.url
      }
    case CREATE_POST_REQUEST:
      return {
        ...state,
        createPostErrorMessage: undefined,
        creatingPost: true
      }
    case CREATE_POST_RECEIVED:
      return {
        ...state,
        createPostErrorMessage: undefined,
        creatingPost: false
      }
    case CREATE_POST_FAILED:
      const setCreatePostFailedAction = (action as CreatePostFailedAction)
      return {
        ...state,
        createPostErrorMessage: setCreatePostFailedAction.errorMessage,
        creatingPost: false
      }
    default:
      return state
  }
}

export default createPostReducer
