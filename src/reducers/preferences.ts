import { SET_LANG_PREFERENCE } from '../actions/action-types'
import { SetLanguagePreferenceAction } from '../actions/preferences'

export interface PreferencesState {
  languages: string[]
}

export const initialState: PreferencesState = {
  languages: []
}

export default (state: PreferencesState = initialState, action: SetLanguagePreferenceAction): PreferencesState => {
  switch (action.type) {
    case SET_LANG_PREFERENCE:
      return {
        ...state,
        languages: action.languages
      }
    default:
      return state
  }
}
