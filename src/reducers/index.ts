import { combineReducers } from 'redux'
import loggedInUserReducer, { LoggedInUserState } from './logged-in-user'
import createPostReducer, { CreatePostState } from './create-post'
import preferencesReducer, { PreferencesState } from './preferences'
import postsReducer, { PostState } from './posts'
import registerUserReducer, { RegisterUserState } from './create-user';

export interface AppState {
  createPost: CreatePostState
  loggedInUser: LoggedInUserState
  posts: PostState
  preferences: PreferencesState
  registerUser: RegisterUserState
}

const rootReducer = combineReducers<AppState>({
  createPost: createPostReducer,
  loggedInUser: loggedInUserReducer,
  posts: postsReducer,
  preferences: preferencesReducer,
  registerUser: registerUserReducer,
})

export default rootReducer
