import { AnyAction } from 'redux'
import {
  LOGIN_REQUEST,
  LOGIN_RECEIVED,
  LOGIN_FAILED,
  LOGOUT_SUCCESS
} from '../actions/action-types'

export const initialState: LoggedInUserState = {
  isAuthenticated: false,
  logining: false,
}

export interface LoggedInUserState {
  isAuthenticated: boolean
  user?: LoggedInUser
  loginErrorMessage?: string
  logining?: boolean
}

interface LoggedInUser {
  email: string
  id: number
  username: string
  languages?: string[]
}

const loggedInUser = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case LOGIN_REQUEST:
      return { ...state, logining: true, loginErrorMessage: undefined }
    case LOGIN_RECEIVED:
      const { user } = action
      return { ...state, logining: false, loginErrorMessage: undefined, isAuthenticated: true, user }
    case LOGIN_FAILED:
      return { ...state, logining: false, loginErrorMessage: action.errorMessage }
    case LOGOUT_SUCCESS:
      return { ...state, isAuthenticated: false, user: undefined }
    default:
      return state
  }
}

export default loggedInUser