import { AnyAction } from 'redux'
import {
  REGISTER_FAILED,
  REGISTER_RECEIVED,
  REGISTER_REQUEST
} from '../actions/action-types'

export const initialState: RegisterUserState = {
  registering: false,
}

export interface RegisterUserState {
  registerErrorMessage?: string
  registering?: boolean
}

const registerUser = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case REGISTER_REQUEST:
      return { ...state, registering: true, registerErrorMessage: undefined }
    case REGISTER_RECEIVED:
      return { ...state, registering: false, registerErrorMessage: undefined }
    case REGISTER_FAILED:
      return { ...state, registering: false, registerErrorMessage: action.errorMessage }
    default:
      return state
  }
}

export default registerUser