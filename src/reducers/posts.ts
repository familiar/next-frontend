import { AnyAction } from 'redux'
import {
  POSTS_SUCCESS,
  VOTE_REQUEST,
  POSTS_REQUEST,
  POSTS_FAILED,
} from '../actions/action-types'

export interface Post {
  id: number
  channel_id: string
  user_id: string
  points: number
  created_date: string
  updated_date: string
  language: string
  title: string
  external_url?: string
  images?: string
  body?: string
  username: string
  vote: number
  image?: string
}

export interface Posts {
  [id: string]: Post
}

export interface PostState {
  ids: number[]
  posts: Posts
  loading?: boolean
  error?: boolean
}

export const initialState: PostState = {
  ids: [],
  posts: {}
}

const postsReducer = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case POSTS_REQUEST:
      return {
        ...state,
        error: false,
        loading: true
      }
    case POSTS_SUCCESS:
      return {
        ...state,
        error: false,
        ids: action.ids,
        loading: false,
        posts: action.posts
      }
    case POSTS_FAILED:
      return {
        ...state,
        error: true,
        loading: false
      }
    case VOTE_REQUEST:
      return {
        ...state,
        posts: {
          ...state.posts,
          [action.postId]: {
            ...state.posts[action.postId],
            points: action.points,
            vote: action.vote,
          }
        }
      }
    default:
      return state
  }
}

export default postsReducer