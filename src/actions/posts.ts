import axios from 'axios'
import {
  VOTE_REQUEST,
  POSTS_REQUEST,
  POSTS_SUCCESS,
  POSTS_FAILED
} from '../actions/action-types'

import { Action, ThunkAction, Dispatch } from '../store'
import { Post, Posts } from '../reducers/posts'
import { trackVote } from '../events';

export interface VotePostAction extends Action {
  postId: number
  points: number
  vote: number
}

export const setPosts = (currentPosts: Post[]) => {
  const postIds: number[] = []
  const posts = currentPosts.reduce((acc: Posts, post) => {
    postIds.push(post.id)
    acc[post.id] = post
    return acc
  }, {})

  return { type: POSTS_SUCCESS, ids: postIds, posts }
}

export const postRequestError = () => ({
  type: POSTS_FAILED
})

const votePost = (postId: number, points: number, vote: number): VotePostAction => ({
  points,
  postId,
  type: VOTE_REQUEST,
  vote,
})

export const fetchLatest = (languages?: string[]): ThunkAction<void> => {
  return async (dispatch: Dispatch, getState) => {
    const state = getState().posts
    if (!state.loading) {
      try {
        dispatch({ type: POSTS_REQUEST })
        const { data: { posts } } = await axios.get('/api/posts/latest', {
          params: {
            languages
          }
        })
        dispatch(setPosts(posts))
      } catch (e) {
        console.error(e)
        dispatch(postRequestError())
      }
    }
  }
}

export const upvote = (postId: number): ThunkAction<void> => {
  return async (dispatch: Dispatch, getState) => {
    const state = getState()
    const selectedPost = state.posts.posts[postId]
    const vote = selectedPost.vote < 1 ? 1 : 0
    const points = selectedPost.points + vote - selectedPost.vote
    dispatch(votePost(postId, points, vote))
    trackVote(postId, vote)

    try {
      await axios.post('/api/vote', { postId, vote })
    } catch (e) {
      console.error(e)
    }
  }
}

export const downvote = (postId: number): ThunkAction<void> => {
  return async (dispatch: Dispatch, getState) => {
    const state = getState()
    const selectedPost = state.posts.posts[postId]
    const vote = selectedPost.vote > -1 ? -1 : 0
    const points = selectedPost.points + vote - selectedPost.vote
    dispatch(votePost(postId, points, vote))
    trackVote(postId, vote)

    try {
      await axios.post('/api/vote', { postId, vote })
    } catch (e) {
      console.error(e)
    }
  }
}