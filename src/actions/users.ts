import axios from 'axios'
import * as R from 'ramda'
import {
  LOGIN_REQUEST,
  LOGIN_RECEIVED,
  LOGIN_FAILED,
  REGISTER_REQUEST,
  REGISTER_RECEIVED,
  REGISTER_FAILED,
} from './action-types'
import { Action, ThunkAction, Dispatch } from '../store'
import { trackLogin, trackSignup } from '../events'

export interface LoginData {
  email: string;
  password: string;
}

export interface LoginAction extends Action {
  user: LoginData
}

export interface SignupData {
  username: string;
  email: string;
  password: string;
}

const loginFailed = (errorMessage: string) => ({
  errorMessage,
  type: LOGIN_FAILED,
})

const registerFailed = (errorMessage: string) => ({
  errorMessage,
  type: REGISTER_FAILED,
})

export const login = (data: LoginData): ThunkAction<void> => {
  return async (dispatch: Dispatch) => {
    dispatch({ type: LOGIN_REQUEST })
    try {
      const { data: { user } } = await axios.post('/api/login', data)
      dispatch({ type: LOGIN_RECEIVED, user })
      trackLogin(user.id, (e) => {
        if (e) {
          console.error('Error sending login metrics', e)
        }
        window.location.href = '/'
      })
    } catch (e) {
      console.error(e)
      dispatch(loginFailed('Email and password not match.'))
    }
  }
}

export const register = (data: SignupData): ThunkAction<void> => {
  return async (dispatch: Dispatch) => {
    dispatch({ type: REGISTER_REQUEST })
    try {
      const { data: { user }} = await axios.post('/api/signup', data)
      dispatch({ type: REGISTER_RECEIVED })

      trackSignup(user.id, (e) => {
        if (e) {
          console.error('Error sending signup metrics', e)
        }
        dispatch(login(R.omit(['username'], data)))
      })
    } catch (e) {
      console.error(e)
      dispatch(registerFailed('Failed to register.'))
    }
  }
}
