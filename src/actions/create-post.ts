import axios from 'axios'
import { Action, ThunkAction, Dispatch } from '../store'
import {
  URL_METADATA_REQUEST,
  URL_METADATA_RECEIVED,
  SET_CREATE_POST_METADATA,
  CREATE_POST_REQUEST,
  CREATE_POST_RECEIVED,
  CREATE_POST_FAILED
} from './action-types'
import { trackCreatePost } from '../events'

export interface UrlMetadata {
  title: string
  language: string
  image?: string
}

export interface CreatePostData {
  title: string
  url: string
  language: string
  channelName: string
  image?: string
}

// Actions
export interface ReceiveUrlMetadataAction extends Action {
  metadata: UrlMetadata
}

export interface SetCreatePostValuesAction extends Action {
  url: string
  metadata: UrlMetadata
}

export interface CreatePostFailedAction extends Action {
  errorMessage: string
}

// Union types
export type CreatePostAction = ReceiveUrlMetadataAction
  | SetCreatePostValuesAction
  | CreatePostFailedAction

// Action creators
export const fetchUrlMetadata = (url: string): ThunkAction<void> => {
  return async (dispatch: Dispatch, getState) => {
    const state = getState()

    if (!state.createPost.fetchingMetadata) {
      dispatch({ type: URL_METADATA_REQUEST })

      try {
        const res = (await axios.post('/api/url-metadata', { url }, { timeout: 3000 })).data
        dispatch(receiveUrlMetadata({
          language: res.language,
          title: res.title,
          image: res.image,
        }))
      } catch (e) {
        console.debug('Failed to fetch url metadata', e)
        dispatch(receiveUrlMetadata({ language: '', title: '' }))
      }
    }
  }
}

export const receiveUrlMetadata = (metadata: UrlMetadata): ReceiveUrlMetadataAction => ({
  metadata,
  type: URL_METADATA_RECEIVED
})

export const setCreatePostMetadata = (url: string, metadata: UrlMetadata): SetCreatePostValuesAction => ({
  metadata,
  type: SET_CREATE_POST_METADATA,
  url
})

export const createPostFailed = (errorMessage: string): CreatePostFailedAction => ({
  errorMessage,
  type: CREATE_POST_FAILED
})

export const createPostReceived = (): Action => ({
  type: CREATE_POST_RECEIVED
})

export const createPost = (data: CreatePostData): ThunkAction<void> => {
  return async (dispatch, getState) => {
    const state = getState()

    if (!state.createPost.creatingPost) {
      dispatch({ type: CREATE_POST_REQUEST })
      try {
        const res = await axios.post('/api/post', data)
        console.debug(`Created post ID: ${res.data.postId}`)
        dispatch(createPostReceived())

        // redirect to latest page
        trackCreatePost(res.data.postId, (e) => {
          if (e) {
            console.error('Error sending create post metrics', e)
            window.location.replace('/latest')
          }
        })
      } catch (e) {
        console.debug('Error creating post', e)
        dispatch(createPostFailed('Error creating post.'))
      }
    }
  }
}
