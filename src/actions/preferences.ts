import { SET_LANG_PREFERENCE } from './action-types'
import { Action, ThunkAction } from '../store/index'
import axios from 'axios'
import * as Cookies from 'js-cookie'

export interface SetLanguagePreferenceAction extends Action {
  languages: string[]
}

export const setLanguagePreference = (languages: string[]): ThunkAction<void> => {
  return async (dispatch, getState) => {
    const state = getState()
    console.debug('Set language preferences', languages)

    if (state.loggedInUser.isAuthenticated) {
      // Intentionally dropped the await statement so that we do not block the dispatch that updates the UI.
      axios.put('/api/user/languages', { languages })
        .then(() => console.log('Updated user\'s languages.'))
        .catch((e) => console.error('Error updating user\'s languages.', e))

    } else {
      const preferences = Cookies.getJSON('preferences') || {}
      preferences.languages = languages
      Cookies.set('preferences', preferences, { expires: 7, domain: 'familiar.one' })
    }

    dispatch({ type: SET_LANG_PREFERENCE, languages })
  }
}
