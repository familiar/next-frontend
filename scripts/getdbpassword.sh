#!/bin/bash

aws ssm get-parameter --name FamiliarDBUserPassword --with-decryption --output json | jq -r '.Parameter.Value'
