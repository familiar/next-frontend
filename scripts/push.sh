#!/usr/bin/env bash

set -u

REPO_NAME=next-frontend

aws ecr describe-repositories --repository-names $REPO_NAME &>/dev/null

if [ "$?" -eq "255" ]; then
  echo "ECR repository $REPO_NAME not found, creating one"

  aws ecr create-repository --repository-name $REPO_NAME

  if [ "$?" -gt "0" ]; then
    exit $?
  fi
fi

$(aws ecr get-login --no-include-email) && \
docker tag next-frontend 935005516588.dkr.ecr.ap-southeast-1.amazonaws.com/next-frontend:latest && \
docker push 935005516588.dkr.ecr.ap-southeast-1.amazonaws.com/next-frontend:latest
