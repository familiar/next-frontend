build:
	docker build -t next-frontend .

push:
	scripts/push.sh

deploy:
	aws ecs update-service \
		--cluster FamiliarECSCluster \
		--service next-frontend \
		--force-new-deployment