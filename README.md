# Familiar Frontend

Frontend for Familiar

## Run locally
1. `npm install`
2. Run Postgres and Redis locally. See ([./db/README.md](./db/README.md))
3. `npm run dev`

## Seed data for development
```bash
# If you have existing data and need to reset, run this first:
psql postgres://dbuser:dbpassword@localhost:5431/familiar -f db/migrations/1.basic_schema.down.sql

# 1. Creating the initial schema
psql postgres://dbuser:dbpassword@localhost:5431/familiar -f db/migrations/1.basic_schema.up.sql

# 2. Seed the default data
npm run seed:dev
```

## Build and Run Docker Image Locally
```bash
make build

docker run -p 3000:3000 -e "NODE_ENV=production" -e "DB_HOST=docker.for.mac.localhost" -e "REDIS_HOST=docker.for.mac.localhost" next-frontend:latest
```

## Deployment
```bash
# Push the image to ECR
export AWS_PROFILE=familiar
make build
make push

# Force new ECS service deployment
make deploy
```

Read [Makefile](Makefile) and our [ECS Service Cloudformation Stack](https://gitlab.com/familiar/infra/blob/master/aws/ecs-service-next-frontend.yaml) for more details.

## Production
- To connect to production DB:
```bash
export AWS_PROFILE=familiar
export DBHOST=apidb.c3g8nx9gn23s.ap-southeast-1.rds.amazonaws.com
export DBUSER=familiardbuser
export DBPASSWORD=$(./scripts/getdbpassword.sh)

psql postgres://$DBUSER:$DBPASSWORD@$DBHOST/apidb

```