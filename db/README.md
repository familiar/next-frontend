## Database Schema Migration

1. Run Postgres and Redis locally with `docker-compose`

```bash
docker-compose up -d
```

2. Create the schema for the first time

```bash
psql postgres://dbuser:dbpassword@localhost:5431/familiar -f migrations/1.basic_schema.up.sql
```

2. Seed the data

```bash
psql postgres://dbuser:dbpassword@localhost:5431/familiar -f migrations/2.seed_data.up.sql
```
