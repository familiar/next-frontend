DROP TABLE IF EXISTS channels;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS user_channel_subscriptions;
DROP TABLE IF EXISTS posts;
DROP TABLE IF EXISTS post_contents;
DROP TABLE IF EXISTS post_votes;
DROP TABLE IF EXISTS comments;
DROP TABLE IF EXISTS comment_contents;
DROP TABLE IF EXISTS comment_votes;

