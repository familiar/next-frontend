CREATE TABLE IF NOT EXISTS channels (
  id SERIAL NOT NULL PRIMARY KEY,
  name TEXT NOT NULL,
  description TEXT NOT NULL,
  languages JSONB,  -- store an array of language codes
  country TEXT,
  region TEXT,
  created_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  updated_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);
CREATE UNIQUE INDEX IF NOT EXISTS channels_name_idx ON channels (name);

CREATE TABLE IF NOT EXISTS users (
  id SERIAL NOT NULL PRIMARY KEY,
  username TEXT NOT NULL,
  email TEXT NOT NULL,
  password TEXT NOT NULL,
  created_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  updated_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  display_name TEXT,
  avatar_url TEXT,
  languages JSONB,  -- an array of language codes user chosen
  country TEXT
);
CREATE UNIQUE INDEX IF NOT EXISTS users_username_idx ON users (username);
CREATE UNIQUE INDEX IF NOT EXISTS users_email_idx ON users (email);

CREATE TABLE IF NOT EXISTS user_channel_subscriptions (
  channel_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL,
  created_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  languages JSONB,  -- an array of language codes user chosen
  PRIMARY KEY (channel_id, user_id)
);

CREATE TABLE IF NOT EXISTS posts (
  id SERIAL NOT NULL PRIMARY KEY,
  channel_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL,
  points INT NOT NULL DEFAULT 0,
  created_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  updated_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  language TEXT NOT NULL DEFAULT 'en',
  image TEXT,
  has_thumbnail BOOLEAN DEFAULT false 
);
CREATE INDEX IF NOT EXISTS posts_channel_id_idx ON posts (channel_id);
CREATE INDEX IF NOT EXISTS posts_user_id_idx ON posts (user_id);
CREATE INDEX IF NOT EXISTS posts_language_idx ON posts (language);
CREATE INDEX IF NOT EXISTS posts_created_date_idx ON posts (created_date DESC);
CREATE INDEX IF NOT EXISTS posts_points_created_date_idx ON posts (points DESC, created_date DESC);

CREATE TABLE IF NOT EXISTS post_contents (
  id SERIAL NOT NULL PRIMARY KEY,
  title TEXT NOT NULL,
  post_id BIGINT NOT NULL,
  created_date TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
  external_url TEXT,
  body TEXT,
  images JSONB
);
CREATE INDEX IF NOT EXISTS post_contents_post_id_created_date_idx ON post_contents (post_id, created_date DESC);
CREATE INDEX IF NOT EXISTS post_contents_external_url_idx ON post_contents (external_url);

CREATE TABLE IF NOT EXISTS post_votes (
  post_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL,
  vote INT NOT NULL,
  created_date TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
  updated_date TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
  PRIMARY KEY (post_id, user_id)
);

CREATE TABLE IF NOT EXISTS comments (
  id SERIAL NOT NULL PRIMARY KEY,
  post_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL,
  points INT NOT NULL DEFAULT 0,
  created_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  updated_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  parent_comment_id BIGINT
);
CREATE INDEX IF NOT EXISTS comments_post_id_created_date_idx ON comments (post_id, created_date DESC);
CREATE INDEX IF NOT EXISTS comments_user_id_created_date_idx ON comments (user_id, created_date DESC);
CREATE INDEX IF NOT EXISTS comments_parent_comment_id_created_date_idx ON comments (parent_comment_id, created_date DESC);

CREATE TABLE IF NOT EXISTS comment_contents (
  id SERIAL NOT NULL PRIMARY KEY,
  comment_id BIGINT NOT NULL,
  body TEXT NOT NULL,
  created_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);
CREATE INDEX IF NOT EXISTS comment_contents_comment_id_created_date_idx ON comment_contents (comment_id, created_date DESC);

CREATE TABLE IF NOT EXISTS comment_votes (
  comment_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL,
  vote INT NOT NULL,
  created_date TIMESTAMP WITHOUT TIME ZONE,
  PRIMARY KEY (comment_id, user_id)
);
