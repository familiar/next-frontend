import * as React from 'react'
import { DispatchProp } from '../src/store'
import { FamiliarAppContext } from './_app'
import { trackLinkClick } from '../src/events'

interface RedirectionProps {
  url: string
  postId: number
}

class Redirection extends React.Component<RedirectionProps & DispatchProp> {
  static async getInitialProps({ req, res, query }: FamiliarAppContext) {
    if (req && res && query) {
      const { u: url, p: postId } = query

      if (url && postId) {
        // todo: maybe validate url and postId against DB?
        return { url, postId }
      } else {
        res.writeHead(303, { Location: '/' })
        res.end()
      }
    } else {
      window.location.href = '/'
    }
  }

  componentDidMount () {
    trackLinkClick(this.props.postId, (e: Error | undefined) => {
      if (e) {
        console.error('Error sending link metrics', e)
      }
      window.location.href = decodeURIComponent(this.props.url)
    })
  }

  render() {
    return <div />
  }
}

export default Redirection