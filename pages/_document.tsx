import Document, { NextDocumentContext, Head, Main, NextScript, DocumentProps } from 'next/document'
import getConfig from 'next/config'
import { ServerStyleSheet } from 'styled-components'
import { getMixpanelSnippet } from '../src/events'

export default class FamiliarDocument extends Document {
  static getInitialProps({ renderPage }: NextDocumentContext): Promise<DocumentProps> | DocumentProps {
    const { publicRuntimeConfig } = getConfig()

    // Setup styled-components
    const sheet = new ServerStyleSheet()
    const page = renderPage(App => props => sheet.collectStyles(<App {...props} />))
    const styleTags = sheet.getStyleElement()
    return { ...page, styleTags, mixpanel: publicRuntimeConfig.mixpanel }
  }

  render() {
    return (
      <html>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta charSet="utf-8" />
          <link rel="stylesheet" href="/_next/static/style.css" />
          {this.props.styleTags}
          <script type="text/javascript" dangerouslySetInnerHTML={{ __html: `${getMixpanelSnippet({ token: this.props.mixpanel.token })}` }} />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}
