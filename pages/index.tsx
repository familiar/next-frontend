import axios from 'axios'
import * as React from 'react'
import { connect } from 'react-redux'
import { message, Row, Col, Alert } from 'antd'
import getConfig from 'next/config'
import { DispatchProp } from '../src/store'
import { FamiliarAppContext } from './_app'
import { PostList } from '../src/components/PostList'
import ChannelCard from '../src/components/ChannelCard'
import { AppState } from "../src/reducers"
import { Posts } from '../src/reducers/posts'
import { setPosts, downvote, upvote, postRequestError, fetchLatest } from '../src/actions/posts'
import { setLanguagePreference } from '../src/actions/preferences'
import { forwardedHeaders } from '../src/util'
import { trackPageView } from '../src/events'

interface PopularPostsProps {
  error?: boolean
  isAuthenticated: boolean
  posts: Posts
  postIds: number[]
  loading?: boolean
  selectedLanguages: string[]
}

class PopularPosts extends React.Component<PopularPostsProps & DispatchProp> {
  static async getInitialProps({ store, req }: FamiliarAppContext) {
    const { publicRuntimeConfig } = getConfig()
    const state = store.getState()

    if (req) {
      try {
        const baseURL = `${publicRuntimeConfig.api.host}:${publicRuntimeConfig.api.port}`
        const postRes = await axios.get('/api/posts/popular', {
          baseURL,
          headers: forwardedHeaders(req),
          params: {
            languages: state.preferences.languages
          }
        })
        const { posts } = postRes.data
        store.dispatch(setPosts(posts))
      } catch (e) {
        console.error(e)
        store.dispatch(postRequestError())
      }
    }
  }

  constructor(props: PopularPostsProps & DispatchProp) {
    super(props)
    message.config({
      duration: 2,
      maxCount: 1,
    })
  }

  componentDidMount() {
    trackPageView('PopularPosts')
  }

  handleLanguageChange = (checkedValues: any[]) => {
    const languages = checkedValues as string[]
    this.props.dispatch(setLanguagePreference(languages))
    this.props.dispatch(fetchLatest(languages))
  }

  showErrorMessage() {
    return message.error(<span>Please <a href="/login">Login</a> and vote</span>)
  }

  onDownvote = (postId: number) => {
    if (!this.props.isAuthenticated) {
      return this.showErrorMessage()
    }

    this.props.dispatch(downvote(postId))
  }

  onUpvote = (postId: number) => {
    if (!this.props.isAuthenticated) {
      return this.showErrorMessage()
    }

    this.props.dispatch(upvote(postId))
  }

  render() {
    const { postIds, posts, loading, error } = this.props

    return (
      <Row gutter={32}>
        <Col xs={24} md={16} lg={18} xxl={19}>
          {
            error
              ? <Alert
                message="Error Listing Posts"
                description="We've got some issue retrieving posts for you, please try again later."
                type="error"
              />
              : <PostList
                onUpvote={this.onUpvote}
                onDownvote={this.onDownvote}
                postIds={postIds}
                posts={posts}
                loading={loading}
              />
          }
        </Col>
        <Col xs={0} md={8} lg={6} xxl={5}>
          <ChannelCard
            channelName="Malaysia"
            isAuthenticated={this.props.isAuthenticated}
            channelDescription="All things about Malaysia."
            languages={[
              { value: 'en', label: 'English' },
              { value: 'zh', label: 'Chinese' },
              { value: 'ms', label: 'Malay' }
            ]}
            selectedLanguages={this.props.selectedLanguages}
            defaultLanguage="en"
            onLanguageChange={this.handleLanguageChange}
          />
        </Col>
      </Row>
    )
  }
}

const mapStateToProps = (state: AppState): PopularPostsProps => {
  return {
    error: state.posts.error,
    isAuthenticated: state.loggedInUser.isAuthenticated,
    loading: state.posts.loading,
    postIds: state.posts.ids,
    posts: state.posts.posts,
    selectedLanguages: state.preferences.languages
  }
}

export default connect(mapStateToProps)(PopularPosts)
