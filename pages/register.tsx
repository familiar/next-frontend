import * as React from 'react'
import { connect} from 'react-redux'
import { Alert } from 'antd'
import { DispatchProp } from '../src/store'
import SignUpForm from '../src/components/SignUpForm'
import { register, SignupData } from '../src/actions/users'
import { AppState } from '../src/reducers'
import { trackPageView } from '../src/events'

interface RegisterProps {
  registerErrorMessage?: string
  registering: boolean
}

class Register extends React.Component<RegisterProps & DispatchProp> {
  componentDidMount() {
    trackPageView('Register')
  }

  handleSignup = (data: SignupData) => {
    this.props.dispatch(register(data))
  }

  render() {
    return (
      <React.Fragment>
        {
          this.props.registerErrorMessage
          && <Alert type="error" message={this.props.registerErrorMessage} showIcon={true} closable={true} />
        }
        <SignUpForm registering={this.props.registering} onSubmit={this.handleSignup} />
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    registerErrorMessage: state.registerUser.registerErrorMessage,
    registering: state.registerUser.registering,
  }
}

export default connect(mapStateToProps)(Register);