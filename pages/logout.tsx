import axios from 'axios'
import * as React from 'react'
import { connect } from 'react-redux'
import getConfig from 'next/config'
import { FamiliarAppContext } from './_app'
import { DispatchProp } from '../src/store'
import { forwardedHeaders } from '../src/util'
import { trackPageView } from '../src/events'

class Logout extends React.Component<DispatchProp> {
  componentDidMount() {
    trackPageView('Logout')
  }

  static async getInitialProps({req, res }: FamiliarAppContext) {
    const { publicRuntimeConfig } = getConfig()

    if (req && res) {
      try {
        const baseURL = `${publicRuntimeConfig.api.host}:${publicRuntimeConfig.api.port}`
        await axios.post('/api/logout', {}, {
          baseURL,
          headers: forwardedHeaders(req)
        })
      } catch (e) {
        console.error(e)
      }

      res.writeHead(303, { Location: '/' })
      res.end()
    } else {
      window.location.href = '/'
    }
  }
}

export default connect()(Logout)