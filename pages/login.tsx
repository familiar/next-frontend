import * as React from 'react'
import { connect } from 'react-redux'
import { Alert } from 'antd'
import { DispatchProp } from '../src/store'
import LoginForm from '../src/components/LoginForm'
import { login, LoginData } from '../src/actions/users'
import { AppState } from '../src/reducers'
import { trackPageView } from '../src/events'

interface LoginProps {
  loginErrorMessage?: string
  logining: boolean
}

class Login extends React.Component<LoginProps & DispatchProp> {
  componentDidMount() {
    trackPageView('Login')
  }

  handleLogin = (data: LoginData) => {
    this.props.dispatch(login(data))
  }

  render() {
    return (
      <React.Fragment>
        {
          this.props.loginErrorMessage
          && <Alert type="error" message={this.props.loginErrorMessage} showIcon={true} closable={true} />
        }
        <LoginForm logining={this.props.logining} onSubmit={this.handleLogin} />
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    loginErrorMessage: state.loggedInUser.loginErrorMessage,
    logining: state.loggedInUser.logining,
  }
}

export default connect(mapStateToProps)(Login);