import * as React from 'react'
import { connect } from 'react-redux'
import { Alert } from 'antd'
import { DispatchProp } from '../src/store'
import CreatePostForm from '../src/components/CreatePostForm'
import { fetchUrlMetadata, setCreatePostMetadata, createPost } from '../src/actions/create-post'
import { AppState } from '../src/reducers'
import { trackPageView } from '../src/events'

interface CreatePostProps {
  isAuthenticated: boolean
  title: string
  url: string
  language: string
  creatingPost?: boolean
  fetchingUrlMetadata?: boolean
  createPostErrorMessage?: string
  image?: string
}

class CreatePost extends React.Component<CreatePostProps & DispatchProp> {
  componentDidMount() {
    trackPageView('CreatePost')
  }

  handleSubmit = () => {
    console.debug('Submitting create post form')
    this.props.dispatch(createPost({
      channelName: 'malaysia',
      image: this.props.image,
      language: this.props.language,
      title: this.props.title,
      url: this.props.url,
    }))
  }

  handleUrlChanged = (url: string) => {
    // URL changed, fetch metadata for the new url.
    this.props.dispatch(fetchUrlMetadata(url))
  }

  handleFormValuesChange = (changedFields: any) => {
    // Update the state in store
    const title = changedFields.title || this.props.title
    const url = changedFields.url || this.props.url
    const language = changedFields.language || this.props.language
    const image = this.props.image
    this.props.dispatch(setCreatePostMetadata(url, { title, language, image }))
  }

  render() {
    if (!this.props.isAuthenticated) {
      return (
        <div>Please <a href="/login">login</a> to create post</div>
      )
    }

    return (
      <React.Fragment>
        {
          this.props.createPostErrorMessage
          && <Alert type="error" message={this.props.createPostErrorMessage} showIcon={true} closable={true} />
        }
        <CreatePostForm
          title={this.props.title}
          url={this.props.url}
          language={this.props.language}
          fetchingUrlMetadata={this.props.fetchingUrlMetadata}
          creatingPost={this.props.creatingPost}
          createPostErrorMessage={this.props.createPostErrorMessage}
          onSubmit={this.handleSubmit}
          onChange={this.handleFormValuesChange}
          onUrlChanged={this.handleUrlChanged}
        />
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state: AppState): CreatePostProps => {
  return {
    createPostErrorMessage: state.createPost.createPostErrorMessage,
    creatingPost: state.createPost.creatingPost,
    fetchingUrlMetadata: state.createPost.fetchingMetadata,
    isAuthenticated: state.loggedInUser && state.loggedInUser.isAuthenticated || false,
    language: state.createPost.metadata ? state.createPost.metadata.language : '',
    title: state.createPost.metadata ? state.createPost.metadata.title : '',
    url: state.createPost.url || '',
    image: state.createPost.metadata && state.createPost.metadata.image,
  }
}

export default connect(mapStateToProps)(CreatePost)
