import * as React from 'react'
import { Provider } from 'react-redux'
import { Store } from 'redux'
import App, { Container, AppComponentContext } from 'next/app'
import { NextContext } from 'next'
import withRedux from 'next-redux-wrapper'
import { Layout } from 'antd'
import initStore from '../src/store/create-store'
import NavBar from '../src/components/Navbar'
import Header from '../src/components/Header'
import Footer from '../src/components/Footer'
import Content from '../src/components/Content'
import { AppState } from '../src/reducers/index'

interface FamiliarAppComponentProps {
  store: Store<AppState>
}

// `store` is injected by `next-redux-wrapper` into `NextContext`
export interface FamiliarAppContext extends NextContext {
  store: Store<AppState>
}

class FamiliarApp extends App<FamiliarAppComponentProps> {
  static async getInitialProps({ Component, ctx }: AppComponentContext) {
    const context = ctx as FamiliarAppContext

    // workaround for missing `Component` type issue
    // https://github.com/zeit/next.js/issues/1651#issuecomment-399284695
    const pageProps = typeof (Component as any).getInitialProps === 'function'
      ? await (Component as any).getInitialProps(context)
      : {}
    return { pageProps }
  }

  render() {
    const { Component, pageProps, store, router } = this.props
    const { loggedInUser } = store.getState()

    if (router.pathname === '/redirection') {
      // Do not render layout for link redirection
      return (
        <Container>
          <Provider store={store}>
            <Component {...pageProps} />
          </Provider>
        </Container>
      )
    }

    return (
      <Container>
        <Provider store={store}>
          <Layout>
            <Header>
              <NavBar selectedPath={router.route} loggedInUser={loggedInUser} />
            </Header>
            <Content>
              <Component {...pageProps} />
            </Content>
            <Footer />
          </Layout>
        </Provider>
      </Container>
    )
  }
}

export default withRedux(initStore)(FamiliarApp)
