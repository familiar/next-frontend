# Build the application with a general NodeJS base image
FROM node:8.12.0 AS build

# Install and cache dependencies
COPY package.json package-lock.json /app/
WORKDIR /app
RUN npm install

# Build the NodeJS app
COPY . /app
RUN npm run build

# Deploy on smaller image
FROM node:8.12.0-alpine

COPY --from=build /app /app
WORKDIR /app

EXPOSE 3000

ENTRYPOINT ["node"]
CMD [ ".next/production-server/index.js" ]
