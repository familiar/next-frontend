const withTypescript = require("@zeit/next-typescript")
const withPlugins = require('next-compose-plugins')
const withCss = require('@zeit/next-css')

const { NODE_ENV = 'development' } = process.env

const publicRuntimeConfig = require(`./config/public/${NODE_ENV}`)
const serverRuntimeConfig = require(`./config/server/${NODE_ENV}`)

const nextConfig = {
  publicRuntimeConfig,
  serverRuntimeConfig
}

if (typeof require !== 'undefined') {
  require.extensions['.css'] = (file) => {}
}

module.exports = withPlugins([withTypescript, withCss], nextConfig)